<div class="releases_board">
  <ul class="column_headers">
    <li class="column column-code">Code</li>
    <li class="column column-project">Project Description</li>
    <li class="column column-countdown">Launch Countdown</li>
    <li class="column column-duedate">Due Date</li>
    <li class="column column-status">Project Status</li>
  </ul>

  <?php foreach($nodes as $node): ?>
    <ul class="project">
      <li class="project_column code"><?= $node->project_tracking_client_code['und'][0]['safe_value'] ?></li>
      <li class="project_column project" data-nid="<?= $node->nid ?>"><?= $node->title ?></li>
      <li class="project_column countdown"><div class="countdown_clock" data-time="<?= $node->project_tracking_release['und'][0]['value'] - time() ?>"></div></li>
      <li class="project_column duedate"><?= date('Y-m-d', $node->project_tracking_release['und'][0]['value']) ?></li>
      <li class="project_column status <?= $node->project_tracking_status['und'][0]['value'] ?>"><?= $node->project_tracking_status['und'][0]['value'] ?></li>
    </ul>
  <?php endforeach; ?>
</div>