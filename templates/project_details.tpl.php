<div class="project_details">
  <div class="employees">
    <div class="team_title">Flight Team</div>
    <?php foreach ($devs as $dev): ?>
      <div class="employee">
        <img src="<?= image_style_url('employee_image', file_load($dev->project_tracking_devs_img['und'][0]['fid'])->uri) ?>">
        <div class="employee_name"><?= $dev->project_tracking_devs_name['und'][0]['safe_value'] ?></div>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="milestones">
    <div class="milestones_title">Flight Progress</div>
    <div class="flight_path"></div>
    <?php $current = true; $left = 10; ?>
    <?php foreach ($milestones as $milestone): ?>
      <div class="milestone_dot <?php
        if ($milestone->project_tracking_milestone_done['und'][0]['value'] == 1) {
          print 'done';
        } elseif ($milestone->project_tracking_milestone_done['und'][0]['value'] == 0 && $current) {
          print 'current';
          $left = $left - 62;
        } else {
          print 'future';
        }
        ?>" style="left: <?= $left . 'px;' ?>"></div>
        <?php
          if ($milestone->project_tracking_milestone_done['und'][0]['value'] == 0 && $current) {
            $current = false;
            $left = ($left + $count) + 62;
          } else {
            $left = $left + $count;
          }
        ?>
    <?php endforeach; ?>
    <div class="milestone_info">
      <?php $current = true; ?>
      <?php foreach ($milestones as $milestone): ?>
        <?php if ($milestone->project_tracking_milestone_done['und'][0]['value'] == 0 && $current): ?>
          <?php $current = false; ?>
          <div class="milestone_title"><?= $milestone->project_tracking_milestone_name['und'][0]['safe_value'] ?></div>
          <div class="milestone_desc"><?= $milestone->project_tracking_milestone_desc['und'][0]['safe_value'] ?></div>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
  </div>
</div>