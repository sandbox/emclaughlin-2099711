(function($, Drupal, window, document, undefined) {
  Drupal.behaviors.project_tracking = {
    attach: function(context) {
      $('.releases_board .countdown_clock').each(function() {
        var clock = $(this).FlipClock(this.getAttribute('data-time'), {
          clockFace: 'DailyCounter',
          countdown: true
        });
      });

      $('.releases_board .project .project').each(function() {
        $(this).on('click', function() {
          $.ajax({
            url: '/get_details?nid=' + this.getAttribute('data-nid'),
            dataType: 'html',
            success: function (data) {
              if ($('.project_details').length > 0) {
                $('.project_details').detach();
              }
              $('.releases_board').append(data);
            }
          });
        });
      });
    }
  }
})(jQuery, Drupal, this, this.document);